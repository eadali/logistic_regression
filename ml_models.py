# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 13:02:34 2019

@author: ERADALI
"""

from numpy import dot, exp, log, copy, hstack, zeros, ones, float32
from numpy import linspace, c_, meshgrid, array, random, absolute
from scipy.optimize import minimize
from matplotlib import pyplot




def plot_contours(ax, x_data, clf):
    """Plot the decision boundaries for a model
    # Arguments
        ax: matplotlib axes object
        x_data: Features of samples
        clf: a classifier
    """
    x_min, x_max = x_data.min() - 0.08, x_data.max() + 0.08
    xx1_data, xx2_data = meshgrid(linspace(x_min, x_max, 100), linspace(x_min, x_max, 100))

    yy_data = clf.predict(c_[xx1_data.ravel(), xx2_data.ravel()])
    yy_data = yy_data.reshape(xx1_data.shape)
    ax.contourf(xx1_data, xx2_data, yy_data<0.5, cmap=pyplot.cm.coolwarm, alpha=0.8)




def plot_scatters(ax, x_data, y_data):
    """Plot the samples data
    # Arguments
        ax: matplotlib axes object
        x_data: Features of samples
        y_data: Labels of samples
    """
    colors = array(['#377eb8', '#ff7f00'])
    ax.scatter(x_data[:,0], x_data[:,1],
               color=colors[((y_data<0.5) + 1) // 2], edgecolors='k')




class LogisticRegression:
    def __init__(self, C=1.0):
        """Inits LogisticRegression object
        # Arguments
            C: Penalty parameter C of the error term.
        """
        self.C = C
        self.coef = None



    def sigmoid(self, x):
        """Sigmoid Function
        # Arguments
            x: Input variable
        # Returns
            Sigmoid function output value
        """
        return 1.0/(1+exp(-x))



    def loss(self, y_true, y_pred):
        """Log loss function
        # Arguments
            y_true: True labels from data
            y_pred: Predicted labels from model
        # Returns
            Loss value
        """
        eps = 1e-15
        y_pred = copy(y_pred.clip(eps, 1-eps))
        loss = 0.5 * (self.coef[:-1,:]**2).sum()
        loss = loss + -self.C*(y_true*log(y_pred) + (1-y_true)*log(1-y_pred)).sum()

        return loss



    def predict(self, x_data):
        """Predict labels function
        # Arguments
            x_data: Features of samples
        # Returns
            Predicted labels
        """
        x_data = copy(hstack((x_data,ones((x_data.shape[0],1)))))
        y_pred = self.sigmoid(dot(x_data, self.coef))

        return y_pred



    def fit(self, x_data, y_data):
        """Trains model for given data
        # Arguments
            x_data: Features of samples
            y_data: Labels of samples
        # Returns
            Optimization result
        """
        y_data = copy(y_data.reshape(-1,1))

        def objective(coef):
            self.coef = coef.reshape(-1,1)
            y_pred = self.predict(x_data)

            return self.loss(y_data, y_pred)

        result = minimize(objective, zeros((x_data.shape[1]+1,1)))
        self.coef = result.x

        return result




class OneClassLR:
    def __init__(self, C=1.0):
        """Inits LogisticRegression object
        # Arguments
            C: Penalty parameter C of the error term.
        """
        self.C = C
        self.coef = None



    def sigmoid(self, x):
        """Sigmoid Function
        # Arguments
            x: Input variable
        # Returns
            Sigmoid function output value
        """
        return 1.0/(1+exp(-x))



    def loss(self, y_true, y_pred):
        """Log loss function
        # Arguments
            y_true: True labels from data
            y_pred: Predicted labels from model
        # Returns
            Loss value
        """
        eps = 1e-15
        y_pred = copy(y_pred.clip(eps, 1-eps))
        loss = 0.5 * (self.coef[:-1,:]**2).sum()
        loss = loss + -self.C*(y_true*log(y_pred) + (1-y_true)*log(1-y_pred)).sum()

        return loss



    def predict(self, x_data):
        """Predict labels function
        # Arguments
            x_data: Features of samples
        # Returns
            Predicted labels
        """
        x_data = copy(hstack((x_data,ones((x_data.shape[0],1)))))
        y_pred = self.sigmoid(dot(x_data, self.coef))

        return y_pred



    def fit(self, x_data):
        """Trains model for given data
        # Arguments
            x_data: Features of samples
            y_data: Labels of samples
        # Returns
            Optimization result
        """
        y_data = ones((x_data.shape[0],1),float32)

        def objective(coef):
            self.coef = absolute(coef.reshape(-1,1))

            ## TODO: remove here
            self.coef

            y_pred = self.predict(x_data)

            return self.loss(y_data, y_pred)

        result = minimize(objective, zeros((x_data.shape[1]+1,1)))
        self.coef = result.x

        return result




if __name__ == '__main__':
    # GENERATE TRAIN DATA
    # =============================================================================
    # Generate train data
    x_data = 0.32 * random.randn(100, 2)
    y_data = ones(100)
    y_data[0:40,] = 0.0

    # Generate some abnormal novel observations
    x_data[:,0] = x_data[:,0] + (1.0-y_data) * 2.0
    x_data[:,1] = x_data[:,1] + (1.0-y_data) * 2.0
    x_data = (x_data - x_data.min(axis=0)) / (x_data.max(axis=0) - x_data.min(axis=0))

    data_fig = pyplot.figure()
    ax = data_fig.add_subplot(111)
    ax.set_title('True Data')
    plot_scatters(ax, x_data, y_data)
    ax.grid()
    # =============================================================================



    # LOGISTIC REGRESSION
    # =============================================================================
    # Create classifier
    lreg = LogisticRegression()

    # Preprocess and train classifier
    lreg.fit(x_data, y_data)

    # Evaluate classifier
    y_pred = lreg.predict(x_data)

    lreg_fig = pyplot.figure()
    ax = lreg_fig.add_subplot(111)
    ax.set_title('Predicted Data')
    plot_contours(ax, x_data, lreg)
    plot_scatters(ax, x_data, y_pred)

    ax.grid()
    # =============================================================================



    # ONE CLASS LOGISTIC REGRESSION
    # =============================================================================
    # Create classifier
    oclr = OneClassLR()

    # Preprocess and train classifier
    oclr.fit(x_data)
    print(oclr.coef)

    # Evaluate classifier
    y_pred = oclr.predict(x_data)

    oclr_fig = pyplot.figure()
    ax = oclr_fig.add_subplot(111)
    ax.set_title('Predicted Data')
    plot_contours(ax, x_data, oclr)
    plot_scatters(ax, x_data, y_pred)

    ax.grid()
    # =============================================================================

    pyplot.show()